package cn.bruce.thread;

import java.util.concurrent.TimeUnit;

/**
 * @author Bruce
 * @create 2021-07-10 16:28
 */

class MyData{
//    int number = 0;
    volatile int number = 0;

    public void addTo60() {
        this.number = 60;
    }

    public void numberPlusPlus() {
        this.number++;
    }
}

/**
 * 1. 验证volatile的可见性
 *  1.1 如果 int number = 0; number前没有volatile关键字修饰，没有可见性
 *  1.2 添加 volatile 后可以解决可见性问题
 * 2. 验证volatile不保证原子性
 *
 */
public class VolatileDemo {
    public static void main(String[] args) {
        MyData myData = new MyData();
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000; j++) {
                    myData.numberPlusPlus();
                }
            },String.valueOf(i)).start();
        }

        // 等待前200000个线程全部计算完成后，再通过main线程获取最终结果值。
        while (Thread.activeCount() > 2) {
            Thread.yield();
        }

        System.out.println(Thread.currentThread().getName() + "\t finally number value:" + myData.number);
    }

    // 测试volatile的可见性
    public void testVolatile1() {
        MyData myData = new MyData();
        // 创建线程
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "\t come in");

            // 线程休眠 3s
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 执行新操作
            myData.addTo60();
            System.out.println(Thread.currentThread().getName() + "\t update number value: " + myData.number);

        },"AAA").start();

        while (myData.number == 0) {
            // main线程循环等待，知道 number 不再等于0
//            System.out.println("waiting...");
        }
        System.out.println(Thread.currentThread().getName() + "\t mission is over");
    }
}
