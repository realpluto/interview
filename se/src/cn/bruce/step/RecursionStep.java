package cn.bruce.step;

/**
 * @author Bruce
 * @create 2021-08-17 17:32
 */
public class RecursionStep {

    public int f(int n) {
        if (n < 1) {
            new IllegalArgumentException("参数不合法");
        } else if (n == 1 || n == 2) {
            return n;
        }
        return f(n - 2) + f(n - 1);
    }

    public static void main(String[] args) {
        RecursionStep step = new RecursionStep();
        long start = System.currentTimeMillis();
        System.out.println(step.f(40));
        long end = System.currentTimeMillis();
        System.out.println("耗时：" + (end - start) + "ms");
    }
}
