package cn.bruce.step;

/**
 * @author Bruce
 * @create 2021-08-17 17:37
 */
public class CircleStep {
    public int f(int n) {
        if (n < 1) {
            new IllegalArgumentException("参数不合法");
        } else if (n == 1 || n == 2) {
            return n;
        }

        int one = 2;
        int two = 1;
        int sum = 0;

        for (int i = 3; i <= n; i++) {
            sum = one + two;
            two = one;
            one = sum;
        }

        return sum;
    }

    public static void main(String[] args) {
        CircleStep step = new CircleStep();
        long start = System.currentTimeMillis();
        System.out.println(step.f(40));
        long end = System.currentTimeMillis();
        System.out.println("耗时：" + (end - start) + "ms");
    }
}
