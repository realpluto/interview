package cn.bruce.transmit;

import java.util.Arrays;

/**
 * @author Bruce
 * @create 2021-08-17 16:47
 */
public class ValueTransmit {
    public static void main(String[] args) {
        int i = 1;
        String str = "hello";
        Integer num = 200;
        int [] arr = {1,2,3,4};
        MyData myData = new MyData();

        change(i, str, num, arr, myData);

        System.out.println("i = " + i);
        System.out.println("str = " + str);
        System.out.println("num = " + num);
        System.out.println("arr = " + Arrays.toString(arr));
        System.out.println("myData.a = " + myData.a);
    }

    public static void change(int j, String s, Integer n, int[] a, MyData m) {
        j += 1;
        s += "word";
        n += 1;
        a[0] += 1;
        m.a += 1;
    }
}

class MyData {
    int a = 1;
}
