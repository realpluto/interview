package cn.bruce.singleton;

import java.io.IOException;
import java.util.Properties;

/**
 * @author Bruce
 * @create 2021-08-17 14:32
 */
public class Hungry3 {

    public static final Hungry3 INSTANCE;
    private String info;

    static {
        Properties pro = new Properties();
        try {
            pro.load(Hungry3.class.getClassLoader().getResourceAsStream("singleton.properties"));
        } catch (IOException e) {
            new RuntimeException(e);
        }

        INSTANCE = new Hungry3(pro.getProperty("info"));
    }

    private Hungry3(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "Hungry3{" +
                "info='" + info + '\'' +
                '}';
    }
}
