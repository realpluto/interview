package cn.bruce.singleton;

/**
 * @author Bruce
 * @create 2021-08-17 14:53
 */
public class Lazy {
    private static Lazy instance;
    private Lazy() {
    }

    public static synchronized Lazy getInstance() {

        if (instance == null) {
            instance = new Lazy();
        }

        return instance;
    }
}
