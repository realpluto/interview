package cn.bruce.singleton;

/**
 * @author Bruce
 * @create 2021-08-17 14:59
 */
public class Lazy2 {
    private Lazy2() {
    }

    private static class Inner {
        private static final Lazy2 INSTANCE = new Lazy2();
    }

    public static Lazy2 getInstance() {
        return Inner.INSTANCE;
    }
}
