package cn.bruce.singleton;

/**
 * @author Bruce
 * @create 2021-08-17 14:21
 */
public class Test {
    public static void main(String[] args) {
        Hungry instance1 = Hungry.INSTANCE;
        Hungry instance2 = Hungry.INSTANCE;
        Hungry instance3 = Hungry.INSTANCE;

        System.out.println(instance1);
        System.out.println(instance2);
        System.out.println(instance3);
        System.out.println("============================");

        Hungry2 hi1 = Hungry2.INSTANCE;
        Hungry2 hi2 = Hungry2.INSTANCE;
        Hungry2 hi3 = Hungry2.INSTANCE;

        System.out.println(hi1);
        System.out.println(hi2);
        System.out.println(hi3);
        System.out.println("============================");

        Hungry3 i1 = Hungry3.INSTANCE;
        Hungry3 i2 = Hungry3.INSTANCE;
        Hungry3 i3 = Hungry3.INSTANCE;

        System.out.println(i1);
        System.out.println(i2);
        System.out.println(i3);
    }
}
