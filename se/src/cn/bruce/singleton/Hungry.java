package cn.bruce.singleton;

/**
 * @author Bruce
 * @create 2021-08-17 14:19
 */
public class Hungry {
    private Hungry() {
    }
    public static final Hungry INSTANCE = new Hungry();
}
